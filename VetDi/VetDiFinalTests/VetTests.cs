﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VetDiFinal.Objects;
using Moq;
using VetDiFinal.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace VetDiFinalTests
{
    [TestClass]
    public class VetTests
    {
        private Vet _vet;
        private List<AnimalRecord> _records;

        private void SetUp()
        {
            _records = new List<AnimalRecord>()
            {
                new AnimalRecord("dog", "hurt", DateTime.Now.AddDays(-2)),
                new AnimalRecord("cat", "sick", DateTime.Now.AddDays(-1)),
                new AnimalRecord("dog", "fleas", DateTime.Now)
            };

            var mock = new Mock<IAnimalRecordRepo>();
            mock.Setup(repo => repo.AnimalRecords).Returns(_records);
            mock.Setup(repo => repo.FindRecordsByAnimalName(It.IsAny<string>())).Returns((string s) => _records.Where(r => r.Name == s).ToList());
            mock.Setup(repo => repo.AddRecord(It.IsAny<AnimalRecord>())).Callback((AnimalRecord r) => _records.Add(r));
            mock.Setup(repo => repo.RemoveRecord(It.IsAny<AnimalRecord>())).Callback((AnimalRecord r) => _records.Remove(r));
            mock.Setup(repo => repo.UpdateRecord(It.IsAny<AnimalRecord>())).Callback((AnimalRecord record) =>
            {
                var actual = _records.First(r => r.ArrivalDateTime == record.ArrivalDateTime);
                var actualIndex = _records.IndexOf(actual);
                _records.Remove(actual);
                _records.Insert(actualIndex, record);
            });
            _vet = new Vet("test", mock.Object);

        }

        [TestMethod]
        public void InitializesCorrectly()
        {
            SetUp();
            Assert.AreEqual(_vet.Name, "test");
        }

        [TestMethod]
        public void CanSeeAllRecords()
        {
            SetUp();
            var records = _vet.SeeAllRecords();
            Assert.AreEqual(records.Count, _records.Count);
        }

        [TestMethod]
        public void CanSeeAllRecordsForName()
        {
            SetUp();
            var records = _vet.SeePatientHistory("dog");
            Assert.AreEqual(records.Count, _records.Count(r => r.Name == "dog"));
        }

        [TestMethod]
        public void CanRediagnose()
        {
            SetUp();
            var record = _records[0];
            _vet.ReDiagnosis(record, "dead");
            Assert.AreEqual(_records[0].Illness, "dead");
        }

        [TestMethod]
        public void CanCheckAnimalIn()
        {
            SetUp();
            _vet.CheckAnimalIn("turtle", "slow");
            Assert.AreEqual(_records.Last().Name, "turtle");
        }

        [TestMethod]
        public void CanBurnTheEvidence()
        {
            SetUp();
            var prevCount = _records.Count;
            var recordToBurn = _records[0];
            _vet.BurnTheEvidence(recordToBurn);
            Assert.AreNotEqual(_records[0], recordToBurn);
            Assert.AreNotEqual(prevCount, _records.Count);
        }
    }
}
