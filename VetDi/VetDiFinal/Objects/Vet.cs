﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VetDiFinal.Repositories;

namespace VetDiFinal.Objects
{
    public class Vet
    {
        public string Name { get; private set; }
        private readonly IAnimalRecordRepo _animalRecordRepo;

        public Vet(string name, IAnimalRecordRepo animalRecordRepo)
        {
            Name = name;
            _animalRecordRepo = animalRecordRepo;
        }

        public AnimalRecord CheckAnimalIn(string name, string initialDiagnosis)
        {
            var record = new AnimalRecord(name, initialDiagnosis, DateTime.Now);
            _animalRecordRepo.AddRecord(record);
            return record;
        }

        public void BurnTheEvidence(AnimalRecord record)
        {
            _animalRecordRepo.RemoveRecord(record);
        }

        public List<AnimalRecord> SeePatientHistory(string name)
        {
            return _animalRecordRepo.FindRecordsByAnimalName(name);
        } 

        public void ReDiagnosis(AnimalRecord record, string actualIllness)
        {
            record.CorrectDiagnosis(actualIllness);
            _animalRecordRepo.UpdateRecord(record);
        }

        public List<AnimalRecord> SeeAllRecords()
        {
            return _animalRecordRepo.AnimalRecords;
        }
    }
}
