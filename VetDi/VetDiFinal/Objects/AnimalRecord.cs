﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetDiFinal.Objects
{
    public class AnimalRecord
    {
        public string Name { get; private set; }

        public  string Illness { get; private set; }

        public DateTime ArrivalDateTime { get; private set; }

        public AnimalRecord(string name, string illness, DateTime arrivalDateTime)
        {
            Name = name;
            Illness = illness;
            ArrivalDateTime = arrivalDateTime;
        }

        public void CorrectDiagnosis(string illness)
        {
            Illness = illness;
        }
    }
}
