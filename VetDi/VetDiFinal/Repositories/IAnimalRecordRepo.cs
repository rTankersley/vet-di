﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VetDiFinal.Objects;

namespace VetDiFinal.Repositories
{
    public interface IAnimalRecordRepo
    {
        List<AnimalRecord> AnimalRecords { get; }

        void AddRecord(AnimalRecord record);

        void RemoveRecord(AnimalRecord record);

        List<AnimalRecord> FindRecordsByAnimalName(string name);

        void UpdateRecord(AnimalRecord animalRecord);
    }
}
