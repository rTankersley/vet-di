﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VetDiFinal.Objects;

namespace VetDiFinal.Repositories
{
    public class FluffyFriendsRecords : IAnimalRecordRepo
    {
        private const string FILE_PATH = @"..\..\..\Data\FluffyFriendsData.txt";

        private string GetRecordString(AnimalRecord record)
        {
            return string.Format("{0};{1};{2}\n", record.Name, record.Illness, record.ArrivalDateTime);
        }

        private AnimalRecord ConvertFromString(string record)
        {
            //Remove the new line and then split on ';'
            var recordData = record.Split(';');
            return new AnimalRecord(recordData[0], recordData[1], DateTime.Parse(recordData[2]));
        }

        public List<AnimalRecord> AnimalRecords
        {
            get
            {
                var records = File.ReadAllLines(FILE_PATH);
                return records.Select(ConvertFromString).ToList();
            }
        }

        public void AddRecord(AnimalRecord record)
        {
            File.AppendAllText(FILE_PATH, GetRecordString(record));
        }

        public List<AnimalRecord> FindRecordsByAnimalName(string name)
        {
            var records = File.ReadAllLines(FILE_PATH);
            var validRecords = records.Where(record => record.StartsWith(name));
            return validRecords.Select(ConvertFromString).ToList();
        }

        public void RemoveRecord(AnimalRecord record)
        {
            var records = File.ReadAllLines(FILE_PATH).ToList();
            var matchingRecord = records.First(r => ConvertFromString(r).ArrivalDateTime == record.ArrivalDateTime);
            records.Remove(matchingRecord);
            File.WriteAllLines(FILE_PATH, records);
        }

        public void UpdateRecord(AnimalRecord animalRecord)
        {
            RemoveRecord(animalRecord);
            AddRecord(animalRecord);
        }
    }
}
