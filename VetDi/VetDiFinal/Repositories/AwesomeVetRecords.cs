﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VetDiFinal.Objects;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Web.Script.Serialization;

namespace VetDiFinal.Repositories
{
    public class AwesomeVetRecords : IAnimalRecordRepo
    {
        private const string FILE_PATH = @"..\..\..\Data\AwesomeVetData.json";

        private string GetJson(AnimalRecord record)
        {
            return new JavaScriptSerializer().Serialize(record);
        }

        public List<AnimalRecord> AnimalRecords
        {
            get
            {
                var records = new List<AnimalRecord>();
                var json = File.ReadAllText(FILE_PATH);
                var animals = new JavaScriptSerializer().DeserializeObject(json);
                foreach(dynamic animal in (animals as object[]))
                {
                    records.Add(new AnimalRecord(animal["Name"].ToString(), animal["Illness"].ToString(), DateTime.Parse(animal["ArrivalDateTime"].ToString())));
                }
                return records;
            }
        }

        public void AddRecord(AnimalRecord record)
        {
            var json = GetJson(record);
            if(!this.AnimalRecords.Any())
            {
                File.WriteAllText(FILE_PATH, string.Format("[{0}]", json));
            }
            else
            {
                var text = File.ReadAllText(FILE_PATH);
                text.Insert(text.Length - 1, "," + json);
            }
        }

        public List<AnimalRecord> FindRecordsByAnimalName(string name)
        {
            throw new NotImplementedException();
        }

        public void RemoveRecord(AnimalRecord record)
        {
            var records = this.AnimalRecords;
            var match = records.First(dt => record.ArrivalDateTime == dt.ArrivalDateTime);
            records.Remove(match);
            WriteRecords(records);
        }

        public void UpdateRecord(AnimalRecord animalRecord)
        {
            RemoveRecord(animalRecord);
            AddRecord(animalRecord);
        }

        private void WriteRecords(List<AnimalRecord> records)
        {
            var json = string.Format("[{0}]", records.Aggregate(string.Empty, (cur, next) => cur + "," + next)).TrimEnd(',');
            File.WriteAllText(FILE_PATH, json);
        }
    }
}
