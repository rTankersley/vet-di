﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VetDiFinal.Objects;
using VetDiFinal.Repositories;

namespace ViConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            DrawHome();

            while(true)
            {
                var homeInput = GetHomeChoice();
                Console.WriteLine();
                if(homeInput == 1)
                {
                    ManageVet(new Vet("Fluffy Friends", new FluffyFriendsRecords()));
                }
                else if(homeInput == 2)
                {
                    ManageVet(new Vet("Awesome Vet", new AwesomeVetRecords()));
                }
                else
                {
                    break;
                }
            }
            
        }

        private static void DrawHome()
        {
            Console.WriteLine("=========================================");
            Console.WriteLine("Welcome to");
            Console.WriteLine("Your Vet Manager! ");
            Console.WriteLine("=========================================");
            Console.WriteLine();
        }

        private static int GetHomeChoice()
        {
            Console.WriteLine("What vet would you like to log in to?");
            Console.WriteLine("1: Fluffy Friends");
            Console.WriteLine("2: Awesome Vet");
            Console.WriteLine("Anything Else: Exit");
            
            switch (GetInput())
            {
                case '1':
                    return 1;
                case '2':
                    return 2;
                default:
                    return 3;
            }
        }

        private static int GetInput()
        {
            var input = Console.Read();
            //Removes junk
            Console.ReadLine();
            return input;
        }

        private static string GetStringInput()
        {
            var input = Console.ReadLine();
            //Removes junk
            return input;
        }

        private static void ManageVet(Vet vet)
        {
            while(true)
            {
                var input = GetVetChoice(vet.Name);
                Console.WriteLine();
                if (input == 1)
                {
                    var name = GetAnimalName();
                    var illness = GetAnimalIllness(name);
                    vet.CheckAnimalIn(name, illness);
                    Console.WriteLine(string.Format("{0} has been checked in!", name));
                    Console.WriteLine();
                }
                else if (input == 2)
                {
                    var name = GetAnimalName();
                    var records = vet.SeePatientHistory(name);

                    if (!records.Any())
                        Console.WriteLine(string.Format("{0} has never been here before.", name));
                    else
                        foreach (var record in records.OrderBy(r => r.ArrivalDateTime))
                        {
                            Console.WriteLine(string.Format("{0}: {1} - {2}", record.ArrivalDateTime, record.Name, record.Illness));
                        }
                    Console.WriteLine();
                }
                else if (input == 3)
                {
                    var records = vet.SeeAllRecords();

                    if (!records.Any())
                        Console.WriteLine("No animals have been here.");
                    else
                        foreach (var animal in records.OrderBy(r => r.Name).ThenBy(r => r.ArrivalDateTime))
                        {
                            Console.WriteLine(string.Format("{0}: {1} - {2}", animal.ArrivalDateTime, animal.Name, animal.Illness));
                        }
                    Console.WriteLine();
                }
                else
                {
                    break;
                }
            }
        }

        private static int GetVetChoice(string vetName)
        {
            Console.WriteLine(string.Format("What vet would you like to do at {0}?", vetName));
            Console.WriteLine("1: Check In Animal");
            Console.WriteLine("2: See Patient History");
            Console.WriteLine("3: See All History");
            Console.WriteLine("Anything Else: Exit");

            switch (GetInput())
            {
                case '1':
                    return 1;
                case '2':
                    return 2;
                case '3':
                    return 3;
                default:
                    return 4;
            }
        }

        private static string GetAnimalName()
        {
            Console.WriteLine("What is the animal's name?");
            return GetStringInput();
        }

        private static string GetAnimalIllness(string name)
        {
            Console.WriteLine(string.Format("What illness does {0} have?", name));
            return GetStringInput();
        }
    }
}
